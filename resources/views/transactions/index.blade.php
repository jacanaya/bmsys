@extends('layouts.layouts') 

@section('title', 'Transaction Form')

@section('content')
	<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Transactions</h2>
            </div> 
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('transactions.create') }}"> Add New Transaction</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>User ID</th>
            <th>Type of Expenses</th>
            <th width="280px">Description</th>
            <th>Amount</th>
        </tr>
    @foreach ($transactions as $key => $item)
    <tr>
        <td>{{ $item->id }}</td>
        <td>{{ $item->user_id }}</td>
        <td>{{ $item->type }}</td>
        <td>{{ $item->description }}</td>
        <td>{{ $item->amount }}</td>
        <td>
            <a class="btn btn-info" href="{{ route('transactions.show', $item->id) }}">Show</a>
            <a class="btn btn-primary" href="{{ route('transactions.edit', $item->id) }}">Edit</a>

            {!! Form::open(['method' => 'DELETE','route' => ['transactions.destroy', $item->id],'style'=>'display:inline']) !!}
            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
        </td>
    </tr>
    @endforeach
    </table>

    

@endsection

@section('scripts')
	<script type="text/javascript">
		
	</script>
@endsection