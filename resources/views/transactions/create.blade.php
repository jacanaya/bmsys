@extends('layouts.layouts')

@section('title', 'Create Transaction Form')

@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Add New Transaction</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('transactions.index') }}"> Back</a>
            </div>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> You have problems in your input<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    
    {!! Form::open(['route' => 'transactions.store', 'method' => 'POST', 'class' => 'form-inline']) !!}
    <?php $userid = Auth::id(); ?>
    
    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Type of Transaction:</strong>
                <br>
                {!! Form::radio('type', 'Income', true) !!} Income <br>
                {!! Form::radio('type', 'Expenses') !!} Expenses
            </div>
        </div>
                {!! Form::hidden('user_id', $userid, array('class' => 'form-control')) !!}

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <br>
                <strong>Description:</strong>
                {!! Form::textarea('description', null, array('placeholder' => 'Description','class' => 'form-control','style'=>'height:100px')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <br>
                <strong>Amount of Money:</strong>
                {!! Form::text('amount', null, array('placeholder' => 'Amount','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Add Transaction</button>
        </div>

    </div>
    {!! Form::close() !!}

@endsection