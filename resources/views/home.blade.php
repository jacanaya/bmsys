@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome to the Budget Management System. You are Logged In. <a href="{{ url('/transactions') }}">Click Here</a> to start transaction.
                </div>
                <!--

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
                -->

                <div class="panel-body">
                    @can('Add Site')
                        <a href="">Add Site </a> <!--<- this link appears because user has the role/permission to edit site -->
                    @endcan
                </div>
            </div>
            @can('Add User')
                <a href="{{ route('users.index') }}">Users </a><!--<- this link appears because user has the role/permission to add user -->
            @endcan
        </div>
    </div>
</div>
@endsection
