<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class UsersTableSeeder extends Seeder
{

public function run()
{
	DB:table('users')->delete();
	User::create(array(
		'name' => 'Jem Canaya',
		'email' => 'jcjcpacer9@gmail.com',
		'password' => Hash::make('123456'),
		'status' => 'true'
	));
}

}