<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/ #Commented for GOT Auth Sample January 31, 2018

Route::get('/', 'ListController@show');
Route::auth();


Route::get('transact', 'TransactionController@index');
Route::post('save', ['uses' => 'TransactionController@create', 'as' => 'save.transactions']);
Route::resource('transactions', 'TransactionController');

//Route::resource('transactions', 'TransactionController@index');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function() {
	Route::resource('users', 'UserController', ['except' => ['create', 'show', 'destroy']]);
	Route::post('updateroles', ['uses' => 'UserController@update_role', 'as' => 'users.update_role']);
});

Route::get('/transactions/transactions', function () {

	$transactions = DB::table('transactions')->get();

	$user = Auth::user();


	return view('transactions', ['trans' => $transactions]);



	//$balance = DB:table('transactions')->where
});

Route::get('/{id}/','TransactionController@show');
Route::get('/{type}/','TransactionController@show');


