<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ListController extends Controller
{
    public function show()
    {
    	$characters = [
    		'Daenerys Targaryen' => 'Emilia Clarke',
    		'Jon Snow' => 'Kit Harington',
    		'Arya Stark' => 'Maisie Williams',
    		'Melisandre' => 'Carice van Houten',
    		'Khal Drogo' => 'Jason Momoa',
    		'Tyrion Lannister' => 'Peter Dinklage',
    		'Ramsay Bolton' => 'Iwan Rheon',
    		'Petyr of Tarth' => 'Gwendoline Christie',
    		'Brienne of Tarth' => 'Gwendoline Christie',
    		'Lord Varys' => 'Conleth Hill'
    	];

    	return view('welcome')->withCharacters($characters); #indicates that we are passing $characters array to a view called welcome.blade.php
    }
}

#TAKE NOTE: THIS SOURCE IS INTENDED FOR LARAVEL AUTH TUTORIAL CONTAINING GOT THEMES (January 31, 2018)
