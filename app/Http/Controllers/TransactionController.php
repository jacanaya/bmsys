<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transactions;
use App\User;
//use App\Item; //Tried this out to find out if it works
use Auth;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        

        $transactions = Transactions::get()->where('user_id', Auth::id());
        return view('transactions.index')->withTransactions($transactions);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('transactions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //!! New Ideas 03/13/18 !!//
        //id, type, and description

        $transactions = New Transactions;

        $transactions->id = $request->id;
        $transactions->user_id = $request->user_id;
        $transactions->amount = $request->amount;
        $transactions->type = $request->type;
        $transactions->description = $request->description;

        $transactions->save();

        return redirect()->route('transactions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //!! New Ideas 03/13/18 !!//
        //$transactions = Transactions::find($id);
        //return view('transactions.show', compact(['id', 'user_id', 'type', 'description', 'amount']));

        //$transactions = Transactions::find($id);
        //return view('transactions.show')->withTransactions($transactions);

        $transactions = Transactions::find($id);
        return view('transactions.show', compact('transactions'));

       // return view('transactions.show', compact('transactions', $transactions));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //!! New Ideas 03/13/18 !!//
        $transactions = Transactions::find($id);
        return view('transactions.edit', compact('transactions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //!! New Ideas 03/13/18 !!//

        $this->validate($request, [
            'type' => 'required',
            'amount' => 'required'
        ]);

        Transactions::find($id)->update($request->all());
        return redirect()->route('transactions.index')->with('success', 'Transactions updated successfully');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //!! New Ideas 03/13/18 !!//

        Transactions::find($id)->delete();
        return redirect()->route('transactions.index')->with('success','Transactions deleted successfully');
    }
}
